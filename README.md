# Carte des stations Météo France 

Affichage des stations d'observations des réseaux du sol de france ([source](https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=93&id_rubrique=32)).

Utilise [Pandas](https://pandas.pydata.org/) et [Folium](https://python-visualization.github.io/folium/latest/)

```
python main.py
```

Fichier [stations.html](stations.html) à ouvrir dans le navigateur.