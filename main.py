import folium
import pandas as pd

# Read data from CSV file
data = pd.read_csv("stations.csv", sep=";")

# Extract latitude and longitude
latitude = data["Latitude"].tolist()
longitude = data["Longitude"].tolist()

# Create a new map centered on France
map = folium.Map(location=[46.227638, 2.213749], zoom_start=6)

# Add markers to the map
for lat, lng, name, id_station, altitude, date_ouverture, pack in zip(
    latitude,
    longitude,
    data["Nom_usuel"],
    data["Id_station"],
    data["Altitude"],
    data["Date_ouverture"],
    data["Pack"],
):
    folium.Marker(
        [lat, lng],
        popup=f"<b>{name}</b><br/>Id_station: {id_station}<br/>Altitude: {altitude}<br/>Date d'ouverture: {date_ouverture}<br/>Pack: {pack}",
    ).add_to(map)


# Display the map
map.save("stations.html")
